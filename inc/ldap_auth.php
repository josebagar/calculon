<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

function ldap_check_credentials($username, $password) {
    //LDAP stuff here.
    $username = trim($username);
    $password = trim($password);

    $ds = ldap_connect('LDAPSERVER:LDAPPORT');

    //Can't connect to LDAP.
    if (!$ds) {
        die("Error: Couldn't connect to the LDAP server, quitting");
    }

    // Connection made -- bind anonymously and get dn for username.
    // BINDRDN should be something like:
    //     cn=CN,ou=OU1,ou=OU2,dc=SUBDOMAIN,dc=DOMAIN,dc=EXTENSION
    // Contact your IT department
    $bind = @ldap_bind($ds, 'BINDRDN', 'PASSWORD');

    //Check to make sure we're bound.
    if (!$bind) {
        die("Error: Anonymous bind to LDAP FAILED, quitting");
    }

    // BASEDN will be something like:
    //      dc=SUBDOMAIN,dc=DOMAIN,dc=EXTENSION
    $search = ldap_search($ds, "BASEDN", "(|(name=" . $username . "))");

    //Make sure only ONE result was returned -- if not, they might've thrown a * into the username.  Bad user!
    $entries = ldap_count_entries($ds, $search);
    if ($entries != 1) {
        die("Error processing username (" . ldap_error($ds) . ") -- please try to login again. (Debug 3)<br/>\n");
    }

    $info = ldap_get_entries($ds, $search);

    //Now, try to rebind with their full dn and password.
    $bind = @ldap_bind($ds, $info[0]['dn'], $password);
    if (!$bind || !isset($bind)) {
        die("Login failed -- please try again. (Debug 4)");
    }

    //Now verify the previous search using their credentials.
    // BASEDN will be something like:
    //      dc=SUBDOMAIN,dc=DOMAIN,dc=EXTENSION
    $search = ldap_search($ds, 'BASEDN', "(|(name=" . $username . "))");

    $info = ldap_get_entries($ds, $search);
    ldap_close($ds);

    if ($username == $info[0]['name'][0]) {
        return TRUE;
    } else {
        return FALSE;
    }

}
