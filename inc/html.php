<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/config.php');
require_once(__DIR__.'/system.php');
require_once(__DIR__.'/analysis.php');

// This function displays the top menu
function html_display_menu() {
    ?>
    <div dojoType="dijit.MenuBar" id="navMenu">
        <div dojoType="dijit.PopupMenuBarItem">
        <span>
            User
        </span>
            <div dojoType="dijit.Menu" id="sessionMenu">
                <div dojoType="dijit.MenuItem" onclick="javascript: window.location='user_add.php'; return 0;">
                    <a href="user_add.php">Add user</a>
                </div>
                <div dojoType="dijit.MenuItem" onclick="javascript: window.location='user_list.php'; return 0;">
                    <a href="user_list.php">List users</a>
                </div>
                <div dojoType="dijit.MenuItem" onclick="javascript: window.location='index.php?logout'; return 0;">
                    <a href="index.php?logout">Logout</a>
                </div>
            </div>
        </div>
        <div dojoType="dijit.PopupMenuBarItem">
        <span>
            Calculón
        </span>
            <div dojoType="dijit.Menu" id="mainMenu">
                <div dojoType="dijit.MenuItem" onclick="javascript: window.location='index.php'; return 0;">
                    <a href="analyze.php">Launch case</a>
                </div>
                <div dojoType="dijit.MenuItem" onclick="javascript: window.location='case_list.php'; return 0;">
                    <a href="case_list.php">Cases</a>
                </div>
            </div>
        </div>
    </div>
    <?php
}

// This functions prints the HTML headers
// focus is the HTML entity to focus, if any
function html_display_header($focus = '', $reload = FALSE) {
global $header_sent;

if (!isset($header_sent) || ($header_sent == 0)) {
?>
<!DOCTYPE html>
<head>
    <title>Calculon</title>
    <?php
    // You'll have to tune this manually...
    if (strstr($_SERVER['REMOTE_ADDR'], '10.7.2') ||
        $_SERVER['REMOTE_ADDR'] == '127.0.0.1' ||
        $_SERVER['REMOTE_ADDR'] == '::1') {
        ?>
    <link rel="icon" type="image/png" href="<?php echo dirname($_SERVER['SCRIPT_NAME']);?>img/calculon.png"/>
        <?php
    } else {
        ?>
    <link rel="icon" type="image/png" href="<?php echo dirname($_SERVER['SCRIPT_NAME']);?>favicon.ico"/>
    <?php } ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php
    if ($reload == TRUE) {
        ?>
        <meta http-equiv="refresh" content="60">
    <?php } ?>
    <meta name="Author" content="Joseba García Etxebarria"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <script type="text/javascript">var counter = 1;</script>
    <link rel="stylesheet" type="text/css" href="inc/dojo/dijit/themes/claro/claro.css"/>
    <script type="text/javascript" src="scripts/script.js"></script>
    <script src="inc/dojo/dojo/dojo.js" djConfig="parseOnLoad: true"></script>
    <script type="text/javascript">
        require(["dijit/form/ComboBox", "dijit/form/Form", "dijit/form/FilteringSelect",
            "dijit/form/Button", "dijit/form/TextBox", "dijit/layout/AccordionContainer",
            "dijit/MenuBar", "dijit/PopupMenuBarItem", "dijit/Menu",
            "dijit/MenuItem", "dijit/PopupMenuItem", "dijit/Dialog",
            "dijit/layout/TabContainer", "dijit/layout/ContentPane",
            "dijit/Tooltip", "dojo/parser"]);
    </script>
</head>
<body class="claro" <?php
if ($focus != '') { ?>onload="javascript: if(document.getElementById('<?php echo $focus; ?>') != null) { document.getElementById('<?php echo $focus; ?>').focus(); } return 0;"<?php } ?>>
<?php
echo '<!--', $_SERVER['HTTP_USER_AGENT'], "-->\n";
$header_sent = 1;
}
// If the user is logged in, show the menu bar
if (user_logged_in() >= 0) {
    html_display_menu();

    return;
}
}


// This function displays the HTML page footer
function html_display_footer() {
global $header_sent, $footer_sent;

// Not sent HTML header? Send it
if (!isset($header_sent) || $header_sent == 0) {
    html_display_header();
}

// Not sent HTML footer? Send it.
if (!isset($footer_sent) || $footer_sent == 0) {
?>
<div id="footer">
    Uses icons by the <a href="http://tango.freedesktop.org/">Tango Project</a>
</div>
</body>
</html>
<?php
}
}

// This function creates the main login form
function html_display_login_form() {
    ?>
    <form action="index.php" method="post" id="login_form">
        <h1>Please, log in</h1>
        <label for="username">User Name:</label><br/>
        <input type="text" dojoType="dijit.form.TextBox" name="username" id="username"/><br/>
        <label for="password">Password:</label><br/>
        <input type="password" dojoType="dijit.form.TextBox" name="password" id="password"/><br/>
        <button type="submit" dojoType="dijit.form.Button" value="Login">Login</button>
    </form>
    <?php
}

// This function creates the file upload form
function html_display_upload_form() {
    // No security check, just get the user's ID
    // (security check should've already hapenned)
    $userid = user_logged_in();
    $caseid = case_new($userid);
    $max_upload_size = system_upload_limit();
    $max_uploaded_files = ini_get('max_file_uploads');
    if (empty($max_uploaded_files)) {
        $max_uploaded_files = 20;
    }
    ?>
    <div id="upload_form">
        <form enctype="multipart/form-data" action="analyze.php" method="POST" id="casefile_form">
            <h1>Choose file to upload</h1>
            <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_upload_size; ?>"/>
            <input type="hidden" id="caseid" name="caseid" value="<?php echo $caseid; ?>"/>
            <label for="Solver">Solver:</label><br/>
            <?php
            html_solver_list_selectable();
            ?>
            <label for="params">Extra params:</label><br/>
            <?php
            html_option_combobox();
            ?>
            <label for="comments">Case comments:</label><br/>
            <textarea id="comments" name="comments" data-dojo-type="dijit/form/SimpleTextarea" rows="6" cols="32"
                      style="width:auto;"></textarea><br/>
            <button dojoType="dijit.form.Button" type="submit">Analyze</button>
        </form>
    </div>

    <div>
        <img id="upload_table_bg_image" src="img/box.png"/>
        <table id="upload_table">
            <thead>
            <tr>
                <th width="60%">Name</th>
                <th width="20%">Size (kB)</th>
                <th width="20%">Status</th>
            </tr>
            </thead>
            <tbody id="upload_table_files">
            </tbody>
        </table>
        <!-- This script will actually handle the HTML5-based upload -->
        <script type="text/javascript" src="scripts/html5upload.js"></script>
    </div>
    <?php
}

// Renders the HTML code for displaying the user addition dialog
function html_display_user_grant_form() {
    ?>
    <form action="user_add.php?add" method="post" id="useradd_form">
        <h1>Access grant form</h1>
        <label for="username">User name:</label><br/>
        <input type="text" dojoType="dijit.form.TextBox" id="username" name="username" maxlength="10"/><br/>
        <button dojoType="dijit.form.Button" type="submit">Add user</button>
    </form>
    <?php
}

// Renders the HTML code for displaying the user addition dialog
function html_display_user_add_form() {
    ?>
    <form action="user_add.php?add" method="post" id="useradd_form">
        <h1>User creation form.</h1>
        <label for="username">New user name:</label><br/>
        <input type="text" dojoType="dijit.form.TextBox" id="username" name="username" maxlength="10"/><br/>
        <label for="passwd">User password:</label><br/>
        <input type="password" dojoType="dijit.form.TextBox" id="passwd" name="passwd"/><br/>
        <label for="passwd2">Again, please:</label><br/>
        <input type="password" dojoType="dijit.form.TextBox" id="passwd2" name="passwd2"/><br/>
        <button dojoType="dijit.form.Button" type="submit">Add user</button>
    </form>
    <?php
}

// Will display a textbox with somt baisc info on the system setup
function html_display_systeminfo() {
    ?>
    <div id="system_info">
        <div id="system_info_title">System info:</div>
        Host name: <?php echo $_SERVER['HTTP_HOST']; ?><br/>
        CPU cores: <?php echo system_count_cpus(); ?><br/>
        64 bit CPU cores: <?php echo system_is_64bits() == TRUE ? 'Yes' : 'No'; ?><br/>
        System RAM: <?php echo system_total_mem(); ?>GiB<br/>
        Free RAM: <?php echo system_free_mem(); ?>GiB<br/>
        Max upload size: <?php echo system_upload_limit() / (1024 * 1024); ?>MiB<br/>
    </div>
    <?php
}

// Displays a selectable list of available solvers
function html_solver_list_selectable() {
    global $solver_script_dir;

    $solvers = glob($solver_script_dir . '/*.solver');

    echo "                <select dojoType=\"dijit.form.FilteringSelect\" id=\"solver\" name=\"solver\">\n";
    // Let's ensure we won't diaplay an empty list
    if (count($solvers) == 0) {
        echo 'No solvers available :(';
    } else {
        foreach ($solvers as $solver) {
            echo '                  ' .
                '<option value="' . basename($solver, '.solver') . '">' .
                basename($solver, '.solver') . '</option>' . "\n";
        }
    }
    echo "                </select>\n<br/>\n";
}

// Displays a selectable list of available options
function html_option_combobox() {
    global $user_id;
    //$options = array('', 'smp=4', 'dmp=4 smp=4', '--parallel=frequency --np=4 -m 20000');
    $options = user_get_params($user_id);

    echo "                <select dojoType=\"dijit.form.ComboBox\" id=\"params\" name=\"params\">\n";
    // Let's ensure we won't diaplay an empty list
    foreach ($options as $option) {
        echo "                  <option>$option</option>\n";
    }
    echo "                </select>\n<br/>\n";
}

// Display the contents of a file in a nice div
// This function is useful for displaying very large files, as it'll only
// download a few bytes from the files initial and final parts
function html_peek_file_contents($fname, $extra = '', $initial_lines = 100, $end_lines = 150) {
    if (!file_exists($fname)) {
        return FALSE;
    }

    // Display the whole file for small ones
    if (filesize($fname) < 1048576) {
        html_display_file_contents($fname, $extra);

        return TRUE;
    }

    echo '<div dojoType="dijit.layout.ContentPane" title="' . basename($fname) . '" id="' . basename($fname) . '_contents" class="file_contents_div" ' . $extra . ">\n";
    passthru('head -n ' . $initial_lines . ' ' . $fname);
    echo "\n\n[Omitted middle of the file, showing the last $end_lines lines]<hr/>\n\n";
    passthru('tail -n ' . $initial_lines . ' ' . $fname);
    echo "</div>\n";
}

// Display the contens of a file in a nice div
function html_display_file_contents($fname, $extra = '') {
    if (!file_exists($fname)) {
        return FALSE;
    }

    $fd = fopen($fname, 'r');
    if ($fd == FALSE) {
        return FALSE;
    }

    echo '<div dojoType="dijit.layout.ContentPane" title="' . basename($fname) . '" id="' . basename($fname) . '_contents" class="file_contents_div" ' . $extra . ">\n";
    fpassthru($fd);
    echo "</div>\n";

    fclose($fd);
}

// This function creates the form that asks for the main analysis file
function html_display_analysis_file_selector($caseid, $solver, $params = '', $comments = '') {
    ?>
    <form action="analyze.php" method="POST" id="analysis_file_form">
        <h1>Choose main analysis file.</h1>
        <input type="hidden" name="solver" value="<?php echo $solver; ?>"/>
        <input type="hidden" name="params" value="<?php echo base64_encode($params); ?>"/>
        <input type="hidden" name="comments" value="<?php echo $comments; ?>">
        <input type="hidden" name="caseid" value="<?php echo $caseid; ?>"/>
        <?php
        $analysis_dir = case_dir($caseid);
        html_file_list_radius($analysis_dir);
        ?>
        <input type="submit" value="Analyze"/>
    </form>
    <?php
}

//This function creates a list of radius buttons with the files in a certain dir
function html_file_list_radius($analysis_dir) {
    $files = glob($analysis_dir . '/*');
    foreach ($files as $file) {
        $fname = basename($file);
        if (is_file($file) && is_readable($file)) {
            echo '<input type="radio" name="main_file" ' .
                'value="' . $fname . '" id="' . $fname . "\" />\n";
            echo '<label for="' . $fname . '">' . $fname . "</label><br />\n";
        }
    }
}
