<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// This global var is used for checking if we've already connected
// to the SQL DB.
// Only functions that use direct SQL commands should use it
$mysql_connected = 0;
$mysql_link = NULL;

// This function connects to the SQL server with the parameters given in
// config.php
function db_connect() {
    global $mysql_server, $mysql_username, $mysql_password, $mysql_database;
    global $mysql_connected, $mysql_link;

    if ($mysql_connected == 1) {
        return TRUE;
    }

    $mysql_link = @new mysqli($mysql_server, $mysql_username,
                              $mysql_password, $mysql_database);
    if($mysql_link->connect_error) {
        die('ERROR: Couldn\'t connect to the MySQL server: ' . $mysql_link->connect_error);
    }

    $mysql_connected = 1;

    return TRUE;
}

// This function does nothing. PHP will disconnect SQL when it finishes its
// execution
function db_disconnect() {
    global $mysql_link;

    $mysql_link->close();
    return TRUE;
}

// This function will escape your string
function db_escape_string($instr) {
    global $mysql_connected, $mysql_link;

    // mysql_real_escape_string won't work otherwise
    if ($mysql_connected == 0) {
        db_connect();
    }

    return $mysql_link->real_escape_string(trim($instr));
}

// This function will run a query in the DB
function db_query($sql_query) {
    global $mysql_connected, $mysql_link;

    // mysql_real_escape_string won't work otherwise
    if ($mysql_connected == 0) {
        db_connect();
    }

    return $mysql_link->query($sql_query);
}

// This function will perform a result-less query in the DB
function db_exec($sql_query) {
    return db_query($sql_query);
}

// This function will return an associative array from $query
function db_fetch_assoc($retval) {
    global $mysql_connected;

    // mysql_real_escape_string won't work otherwise
    if ($mysql_connected == 0) {
        db_connect();
    }

    if (!$retval) {
        return FALSE;
    }

    return $retval->fetch_assoc();
}

// Return the id of the last inserted row
function db_insert_id() {
    global $mysql_link;

    return $mysql_link->insert_id;
}

// Return a string representation of the MySQL error
function db_error() {
    global $mysql_link;

    return $mysql_link->error;
}