<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/misc.php');

// Return the numeric value in /proc/meminfo that corresponds
// to $keyval
function parse_meminfo($keyval) {
    if (!is_string($keyval) || empty($keyval)) {
        return 'Unkown';
    }

    $mem = 0;

    // Open the proc filesystem file about our mem
    $fd = fopen('/proc/meminfo', 'r');

    if ($fd == FALSE) {
        return 'Unknown';
    }

    // Line would be like "MemTotal:     16397528 kB"
    while (!feof($fd)) {
        $line = fgets($fd);
        if (strpos($line, $keyval) !== FALSE) {
            // Split the line between spaces
            $elems = explode(' ', $line);
            $n = count($elems);
            if ($n < 3) {
                $mem = 'Unknown';
                break;
            }
            $mem = intval($elems[($n - 2)]);
            break;
        }
    }

    fclose($fd);

    return $mem;
}

// Return the value in /proc/cpuinfo that corresponds
// to $keyval
function parse_cpuinfo($keyval) {
    if (!is_string($keyval) || empty($keyval)) {
        return 'Unkown';
    }

    $feature = 'Unknown';

    // Open the proc filesystem file about our CPU
    $fd = fopen('/proc/cpuinfo', 'r');

    if ($fd == FALSE) {
        return 'Unknown';
    }

    // Line would be like "wp              : yes"
    while (!feof($fd)) {
        $line = fgets($fd);
        if (strpos($line, $keyval) !== FALSE) {
            // Split the line between spaces
            $elems = explode(':', $line);
            $n = count($elems);
            if ($n < 2) {
                $feature = 'Unknown';
                break;
            }
            $feature = trim($elems[1]);
            break;
        }
    }

    fclose($fd);

    return $feature;
}

// Will return the number of available physical CPUs
// Must be used in LINUX systems that have
// /proc/cpuinfo available
function system_count_cpus() {
    $cpus = 0;

    // Open the proc filesystem file about our CPU
    $fd = fopen('/proc/cpuinfo', 'r');

    if ($fd == FALSE) {
        return 'Unknown';
    }

    // Line would be: "processor       : 1"
    while (!feof($fd)) {
        $line = fgets($fd);
        if (strpos($line, 'processor') !== FALSE) {
            $cpus++;
        }
    }

    fclose($fd);

    // Something weird happened
    if ($cpus <= 0) {
        $cpus = 1;
    }

    return $cpus;
}

// Determine if the CPU supports 64 bit instructions
// I won't care about your OS, though
function system_is_64bits() {
    $flags = parse_cpuinfo('flags');
    $flags = explode(' ', $flags);
    if ($flags === FALSE || !is_array($flags)) {
        return 'Unkown';
    }

    if (in_array('lm', $flags)) {
        return TRUE;
    }

    return FALSE;
}

// Will return the maximum data size that can be uploaded in a POST form
function system_upload_limit() {
    $max_upload_size = min(let_to_num(ini_get('post_max_size')), let_to_num(ini_get('upload_max_filesize')));

    return $max_upload_size;
}

// Will return the available free memory
function system_free_mem() {
    $mem = parse_meminfo('MemFree');
    $cached = parse_meminfo('Cached');
    if (is_numeric($mem) && is_numeric($cached)) {
        $mem += $cached;
        $mem /= 1024 * 1024;
        $mem = round($mem, 2);
    }

    return $mem;
}

// Will return the available total memory (in GiB)
function system_total_mem() {
    $mem = parse_meminfo('MemTotal');
    if (is_numeric($mem)) {
        $mem /= 1024 * 1024;
        $mem = round($mem, 2);
    }

    return $mem;
}

?>
