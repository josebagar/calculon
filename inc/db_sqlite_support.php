<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// This global var is used for checking if we've already connected
// to the SQL DB.
// Only functions that use direct SQL commands should use it
$sqlite_connected = 0;
$sqlite_dbh = NULL;

// This function connects to the SQL server with the parameters given in
// config.php
function db_connect() {
    global $sqlite_db;
    global $sqlite_connected;
    global $sqlite_dbh;

    if ($sqlite_connected == 1) {
        return TRUE;
    }

    $sqlite_dbh = new SQLite3($sqlite_db);
    if (!$sqlite_dbh) {
        die('ERROR: Couldn\'t connect to the SQLite server: ' . $sqlite_dbh->lastErrorMsg());
    }

    $sqlite_connected = 1;

    return TRUE;
}

// This function closes the connection to the SQLite database
function db_disconnect() {
    global $sqlite_dbh, $sqlite_connected;

    $sqlite_connected = 0;

    return $sqlite_dbh->close();
}

// This function will try to escape your string to prevent SQL injection attacks
function db_escape_string($instr) {
    global $sqlite_connected, $sqlite_dbh;

    // mysql_real_escape_string won't work otherwise
    if ($sqlite_connected == 0) {
        db_connect();
    }

    return $sqlite_dbh->escapeString(trim($instr));
}

// This function will run a query in the DB
function db_query($sql_query) {
    global $sqlite_connected, $sqlite_dbh;

    // mysql_real_escape_string won't work otherwise
    if ($sqlite_connected == 0) {
        db_connect();
    }

    return $sqlite_dbh->query($sql_query);
}

// This function will run a query in the DB
function db_exec($sql_query) {
    global $sqlite_connected, $sqlite_dbh;

    // mysql_real_escape_string won't work otherwise
    if ($sqlite_connected == 0) {
        db_connect();
    }

    return $sqlite_dbh->exec($sql_query);
}

// This function will return an associative array from $query
function db_fetch_assoc($retval) {
    global $sqlite_connected;

    // mysql_real_escape_string won't work otherwise
    if ($sqlite_connected == 0) {
        db_connect();
    }

    if (!$retval) {
        return FALSE;
    }

    return $retval->fetchArray(SQLITE3_ASSOC);
}

// Return the id of the last inserted row
function db_insert_id() {
    global $sqlite_connected, $sqlite_dbh;

    // mysql_real_escape_string won't work otherwise
    if ($sqlite_connected == 0) {
        db_connect();
    }

    return $sqlite_dbh->lastInsertRowID();
}

// Return a string representation of the MySQL error
function db_error() {
    global $sqlite_connected, $sqlite_dbh;

    // mysql_real_escape_string won't work otherwise
    if ($sqlite_connected == 0) {
        db_connect();
    }

    return $sqlite_dbh->lastErrorMsg();
}
