<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Read the first line of the description file (if it exists)
// It should contain the short description of the document
function ref_shortdesc($id) {
    global $refdir;

    $reflist = glob($refdir . '*.pdf');

    if (is_readable($reflist[$id] . '.description')) {
        $fd = fopen($reflist[$id] . '.description', 'r');
        $shortdesc = trim(fgetss($fd)); // We just want to read the first line

        fclose($fd);

        return $shortdesc;
    }

    return FALSE;
}

// Read the second line of the description file (if it exists)
// It should contain the author/s of the document
function ref_authors($id) {
    global $refdir;

    $reflist = glob($refdir . '*.pdf');

    if (is_readable($reflist[$id] . '.description')) {
        $fd = fopen($reflist[$id] . '.description', 'r');
        fgets($fd);
        $authors = trim(fgetss($fd)); // We want to read the SECOND line

        fclose($fd);

        return $authors;
    }

    return FALSE;
}

function ref_longdesc($id) {
    global $refdir;

    $reflist = glob($refdir . '*.pdf');

    if (is_readable($reflist[$id] . '.description')) {
        $fd = fopen($reflist[$id] . '.description', 'r');
        fgets($fd);
        fgets($fd);    //We ignore the first&second lines
        // Read the rest of the file, and concatenate it, keeping line-breaks
        $longdesc = '';
        while (!feof($fd)) {
            $longdesc .= trim(fgetss($fd)) . "\n";
        }

        fclose($fd);

        return trim($longdesc);
    }

    return FALSE;
}

?>
