<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/config.php');
require_once(__DIR__.'/sql.php');
require_once(__DIR__.'/user.php');
require_once(__DIR__.'/misc.php');

// Will clean up old cases which have been initialized (when accessing
// the analyze form) but haven't been run.
// A new case entry is created in the DB every time a user accesses the
// file upload form, so we need to regularly remove those from the DB
// so that it won't fill with crap
function case_cleanup() {
    global $solver_gracetime;

    // Compute the max creation time for jobs to delete
    $time = time()-$solver_gracetime;

    $time = db_escape_string($time);
    $query = 'SELECT id FROM cases WHERE creation_date_unix < '.$time.' AND status = 0';

    // Perform the query, then remove the cases
    $retval = db_query($query);
    if($retval === FALSE) {
        return -1;
    }

    // Get the result and compare it to the given value
    while( ($case = db_fetch_assoc($retval)) !== FALSE ) {
        case_remove($case['id']);
    }
}

// Will create a new empty case for the user, then it'll return the
// new case ID as matching the DB entry
function case_new($userid) {
    global $solver_job_dir;
    // Create the directory which will hold the case files
    // tempnam gives us a file but we want a directory, so we'll erase it
    // and create a directory in its place
    $analysis_dir = tempnam($solver_job_dir, 'calculon_');
    unlink($analysis_dir);
    $retval = mkdir($analysis_dir, 0777);
    if($retval == FALSE) {
        return -1;
    }
    chmod($analysis_dir, 0777);
    
    // Create an empty list of files, the store it to a string
    $files = array();
    $files = serialize($files);
    
    // Construct & execute the query
    $userid = db_escape_string($userid);
    $query  = 'INSERT INTO cases (creation_date_unix, creator_userid, dir, uploaded_files) VALUES ('.
                time().','.$userid.',"'.$analysis_dir.'","'.$files.'")';
    $retval = db_query($query);
    if($retval == FALSE) {
        return -2;
    }
    
    // Return the ID of the recently created row
    return db_insert_id();
}

// Will remove the given case entry from the database
// and from disk (if any)
// Return <0 on error, 0 otherwise
function case_remove($caseid) {
    global $solver_job_dir;

    // A few checks...
    $caseid = intval($caseid);
    if($caseid < 0) {
        return -1;
    }
    
    // Get the case dir and remove it, trying to prevent
    // doing nasty stuff
    $casedir = case_dir($caseid);
    if($casedir == '' || dirname($casedir) != $solver_job_dir) {
        return -2;
    }
    deltree($casedir);

    // Construct & execute the query
    $caseid = db_escape_string($caseid);
    $query  = 'DELETE FROM cases WHERE id='.$caseid;

    $retval = db_query($query);
    if($retval == FALSE) {
        return -2;
    }
    
    // Return the ID of the recently created row
    return 0;
}

// Check wether given case belongs to given user
function case_belongs_to($caseid, $userid) {
    // A few checks...
    if(! is_numeric($caseid) || $caseid < 0) {
        return FALSE;
    }
    
    if(! is_numeric($userid) || $userid < 0) {
        return FALSE;
    }

    // Construct the query, then execute it
    $userid = db_escape_string($userid);
    $caseid = db_escape_string($caseid);
    $query  = 'SELECT id FROM cases WHERE creator_userid='.$userid.' AND id='.$caseid.' LIMIT 1';
    $retval = db_query($query);
    if($retval == FALSE) {
        return FALSE;
    }
    
    // Get the result and compare it to the given value
    $dbid = db_fetch_assoc($retval);
    if($dbid['id'] == $caseid) {
        return TRUE;
    }
    
    // They didn't match
    return FALSE;
}

// Given a case id, return its associated directory
function case_dir($caseid) {
    // A few checks...
    if(! is_numeric($caseid) || $caseid < 0) {
        return "";
    }
    
    // Construct the query, then execute it
    $caseid = db_escape_string($caseid);
    $query  = 'SELECT dir FROM cases WHERE id='.$caseid.' LIMIT 1';
    $retval = db_query($query);
    if($retval == FALSE) {
        return "";
    }
    
    // Get the result and compare it to the given value
    $dir = db_fetch_assoc($retval);
    return $dir['dir'];
}

// Given a case id return the list of uploaded files
// Will only return files that actually exist in the FS
function case_get_uploaded_files($caseid) {
    // A few checks...
    if(! is_numeric($caseid) || $caseid < 0) {
        return -1;
    }

    $caseid   = db_escape_string($caseid);
    
    $query  = "SELECT uploaded_files FROM cases WHERE id=".$caseid;
    $retval = db_query($query);
    if($retval == FALSE) {
        return -2;
    }
    
    // Fetch the list of currently available files as an array
    $db_files = db_fetch_assoc($retval);
    $db_files = unserialize($db_files['uploaded_files']);
    if(! is_array($db_files)) {
        return -3;
    }

    // Get the case directory
    $dir = case_dir($caseid);

    // Build the array with the existing files
    $files = array();
    foreach($db_files as $db_file) {
        if(file_exists($dir.'/'.$db_file)) {
            $files[] = $db_file;
        }
    }

    return $files;
}

// Given a case id and a file path, associate it to the case
// This function will not actually look in your FS to check if
// the files reported in the DB are actually there
function case_add_file($caseid, $fname) {
    // A few checks...
    if(! is_numeric($caseid) || $caseid < 0 || empty($fname)) {
        return -1;
    }

    // Construct the query, then execute it
    $caseid   = db_escape_string($caseid);
    $fname    = db_escape_string($fname);
    
    $query  = "SELECT uploaded_files FROM cases WHERE id=".$caseid;
    $retval = db_query($query);
    if($retval == FALSE) {
        return -2;
    }
    
    // Fetch the list of currently available files as an array
    $files = db_fetch_assoc($retval);
    $files = unserialize($files['uploaded_files']);
    if(! is_array($files)) {
        return -3;
    }
    
    // If the file already exists in the array, return happily
    if(in_array($fname, $files)) {
        return TRUE;
    }
    
    // Add the file path to the array, then push it back to the DB
    $files[] = $fname;
    $files   = serialize($files);
    $query   = 'UPDATE cases SET uploaded_files=\''.$files.'\' WHERE id='.$caseid;
    $retval  = db_query($query);
    if($retval == FALSE) {
        return -4;
    }
    
    return TRUE;
}

// Pretty basic, huh?
function analyze($username, $caseid, $filename, $solver, $params='') {
    // Sanitize the given params
    if(! file_exists($filename)) {
        echo "<h1>ERROR:</h1>";
        echo "Analysis file (".$filename.") not correctly uploaded";
        die();
    }
    $params = str_replace('&', '', $params);
    $params = str_replace(';', '', $params);
    $params = str_replace('<', '', $params);
    $params = str_replace('>', '', $params);
    exec('"'.$solver.'" '.$username.' '.$caseid.' '.$filename.' '.$params.' > '.dirname($filename).'/stdout'.' 2>&1 &');
}
