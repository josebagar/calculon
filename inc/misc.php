<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Random auxiliary functions

// This function transforms the php.ini notation for numbers (like '2M')
// to an integer (2*1024*1024 in this case)
function let_to_num($v) {
    $l = substr($v, -1);
    $ret = substr($v, 0, -1);
    switch (strtoupper($l)) {
        case 'P':
            $ret *= 1024;
            break;
        case 'T':
            $ret *= 1024;
            break;
        case 'G':
            $ret *= 1024;
            break;
        case 'M':
            $ret *= 1024;
            break;
        case 'K':
            $ret *= 1024;
            break;
    }

    return $ret;
}

// Delete a dir and its contents
function deltree($dir) {
    $files = array_diff(scandir($dir), array('.', '..'));
    // Remove all the files first
    // If there are subdirectories, delete them first
    foreach ($files as $file) {
        if (is_dir($dir . '/' . $file)) {
            deltree($dir . '/' . $file);
        } else {
            unlink($dir . '/' . $file);
        }
    }

    if (is_dir($dir)) {
        rmdir($dir);
    }
}
