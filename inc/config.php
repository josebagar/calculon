<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// LOGIN TYPE
// ===================================
// Can be either "local", "ldap" or "cached"
// - "local" means using passwords stored in the SQL DB specified
//   below. Users must be manually created
// - "ldap" means logging in through a LDAP server.
//   Users must still be granted access manually
$authtype = 'local';

// DB OPTIONS
// ===================================
// Choose DB server type. Currently supported: 'mysql', 'sqlite'
$db_server = 'sqlite';
// SQLite data (directory, database names...)
// If you're using a SQLIte DB server, fill in the gaps here
// All paths relative to the "inc" directory and remember that
// Apache must be able to read&write to the DB files and to
// the dir on top of them
$sqlite_db = 'dbs/calculon.sqlite';
// MySQL server, username, password & DB name
// If you're using a MySQL server, fill in the gaps here
$mysql_server = 'localhost';
$mysql_username = 'calculon';
$mysql_password = 'YOURPASSWORDHERE';
$mysql_database = 'calculon';

// SOLVER CONTROL OPTIONS
// ===================================
// The path to the dir where all the solvers are stored
$solver_script_dir = 'solvers';
// The patch where solver jobs will be placed (no trailing slashes)
$solver_job_dir = '/scratch';
// Time before an automatically created job is deleted (seconds)
// If your HTTP and MySQL servers aren't configured to the same timezone,
// adjust accordingly or havoc will happen
$solver_gracetime = 300;

// ANALYSIS TRACKING OPTIONS
// ===================================
// Files with any of these extensions will appear
// in the analysis_status view
// But remember that extensions are case-sensitive
$preview_extensions = array('f06', 'f04', 'OUT', 'sts', 'log', 'req');
// Files with this extension will be initially shown
$preview_default_ext = 'f06';
// Number of lines to show of each of the files when showing
// a reduced version of the file
// The code will show the first $preview_length lines, then
// the last $preview_length lines.
$preview_length = 200;

// Comment in production systems
//error_reporting(E_ALL);
