<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/* This file contains the functions used to use the SQL database.
   Right now, the DB system is based on MySQL, but this file should provide
   an abstraction layer so the rest of the system shouldn't know about DBs.
   
   See docs/schema_mysql.sql for a script that can be used to create a
   MySQL/MariaDB DB.
 */
require_once(__DIR__.'/config.php');

if ($db_server == 'mysql') {
    require_once(__DIR__.'/db_mysql_support.php');
} elseif ($db_server == 'sqlite') {
    require_once(__DIR__.'/db_sqlite_support.php');
} else {
    die("<h1>Must configure a valid DB server</h1>");
}
