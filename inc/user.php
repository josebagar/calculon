<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/config.php');
require_once(__DIR__.'/sql.php');
require_once(__DIR__.'/ldap_auth.php');

// Checks that the username matches his password from the SQL DB
// Returns <0 on error or userid on case of success.
//   -1: couldn't execute query (is the DB correctly configured?)
//   -2: usernmae doesn't exist in DB
//   -3: username and password don't match
function user_check_credentials($username, $password) {
    global $authtype;

    if ($username == '' || $password == '') {
        return -1;
    }

    db_connect();  //Connect to the SQL server
    // Try to prevent SQL injection attacks
    $username = db_escape_string($username);
    $password = db_escape_string($password);

    // Run the query for the passwd hash
    // Only accept one result, ordered by userid
    $retval = db_query('SELECT userid, hash FROM users WHERE username="' . $username . '" LIMIT 1');
    if (!$retval) {
        return -2;
    }

    // Try to get the user's password hash value
    $retval = db_fetch_assoc($retval);
    if (!$retval) {
        return -3;
    }

    if ($authtype == 'local') {
        // Check the hash value in the DB matches the one given
        if(password_verify($password, $retval['hash'])) {
            return $retval['userid'];
        }

        return -4;
    } elseif ($authtype == 'ldap') {
        // Check against the LDAP server, then return the database user ID (if successful)
        if (ldap_check_credentials($username, $password) == TRUE) {
            return $retval['userid'];
        }

        return -4;

        // Get the user id
    }
}

// Check if the user's session is still valid.
// Returns TRUE on success and <0 in case of error
//  -1: SQL query cannot be executed
//  -2: userid cannot be found
//  -3: session_id invalid
//  -4: Session expired
//  -5: user's IP is not valid
function user_check_session($userid, $session_id) {
    if (!$userid || !$session_id) {
        return -1;
    }

    db_connect();  // Connect to the SQL server, if not already done

    $retval = db_query('SELECT session_id, expiration_date, user_ip FROM ' .
                       'users WHERE userid=' . $userid . ' LIMIT 1'); //We trust $userid
    if (!$retval) {
        return -2;
    }

    // Try to get the user's data
    $retval = db_fetch_assoc($retval);
    if (!$retval) {
        return -3;
    }

    // Check the session ID
    if ($retval['session_id'] != $session_id) {
        return -4;
    }

    // Check if the session hasn't yet expired
    if (time() >= $retval['expiration_date']) {
        return -5;
    }

    // Check wether the  user's IP is the same they logged in with
    if ($_SERVER['REMOTE_ADDR'] != $retval['user_ip']) {
        return -6;
    }

    // All the checks were correct, proceed
    return TRUE;
}

// Log in the currently online user, given username and password.
// Returns <0 on error TRUE on success.
//  -1: Headers already sent, cannot set cookie.
//  -2: Username and password verification failed
//  -3: SQL query failed.
function user_log_in($username, $passwd) {
    if (!$username || !$passwd) {
        return -1;
    }

    db_connect();

    // Session couldn't be started
    if (headers_sent()) {
        return -2;
    }

    // Get the user's ID in the database
    $userid = user_check_credentials($username, $passwd);
    if ($userid < 0) {
        return -3;
    }

    // Create the session vars & set the user's cookie
    $session_id = password_hash($_SERVER['REMOTE_ADDR'] . time(), PASSWORD_BCRYPT);
    $expiration_date = time() + 24 * 60 * 60;
    $user_ip = $_SERVER['REMOTE_ADDR'];
    setcookie('session_id', $session_id, 0);  /* Might fail! */
    setcookie('user_id', $userid, 0);

    // Write the session vars into the DB
    $retval = db_query('UPDATE users SET ' .
                       'session_id="' . $session_id . '"' .
                       ', expiration_date=' . $expiration_date .
                       ', user_ip="' . $user_ip . '" WHERE userid=' . $userid);
    if ($retval == FALSE) {
        return db_error();
    }

    return TRUE;
}

// Log out current user. Returns TRUE on success, <0 on error
//  -1: User hasn't logged in.
//  -2: SQL query failed.
function user_log_out() {
    db_connect();

    if (($user_id = user_logged_in()) < 0) {
        return -1;
    }

    /* This is the equivalent to deleting the user's cookie
       The UA might not accept the cookie! */
    if (headers_sent() == FALSE) {
        setcookie('session_id', FALSE, time() - 24 * 60 * 60);
        setcookie('user_id', FALSE, time() - 24 * 60 * 60);
    }

    // Unset the session id in the DB
    if (db_query('UPDATE users SET session_id=NULL, expiration_date=NULL WHERE ' .
                 'userid=' . $user_id) == FALSE
    ) {
        return -2;
    }

    return TRUE;
}

// Checks wether the current user has been logged in.
// Returns user_id on success, <0 on error
//  -1: session_id cookie not set
//  -2: user_id cookie not set
//  -3: user NOT logged in
function user_logged_in() {
    if (!isset($_COOKIE['session_id'])) {
        return -1;
    }

    if (!isset($_COOKIE['user_id'])) {
        return -2;
    }

    $session_id = $_COOKIE['session_id'];
    $user_id = $_COOKIE['user_id'];

    if (user_check_session($user_id, $session_id) > 0) {
        return $user_id;
    }

    return -3;
}

// Gets the username matching the given userid
// Returns a string in case of success and <0 in case
// of failure.
//   -1: SQL query failed.
//   -2: No users found matching that ID
function user_name($userid) {
    if (!$userid) {
        return -1;
    }

    db_connect();

    // Remember we don't trust the user
    $userid = db_escape_string($userid);
    $retval = db_query("SELECT username FROM users where " .
                       "userid='" . $userid . "' LIMIT 1");
    if (!$retval) {
        return -2;
    }

    $retval = db_fetch_assoc($retval);
    if (!$retval) {
        return -3;
    }

    return $retval['username'];
}

// Adds the current user to the DB
function user_add($username, $password) {
    db_connect();

    // Prevent SQL injection attacks
    $username = db_escape_string($username);
    $password = db_escape_string($password);
    $hash = password_hash($password, PASSWORD_BCRYPT);

    $retval = db_query("INSERT into users (username, hash) VALUES " .
                       "('" . $username . "', '" . $hash . "')");
    if (!$retval) {
        return -1;
    }

    return TRUE;
}

// Delete the user from the DB
function user_del($username) {
    db_connect();

    // Prevent SQL injection attacks
    $username = db_escape_string($username);

    $retval = db_exec("DELETE FROM users WHERE username='" . $username . "'");
    if (!$retval) {
        return -1;
    }

    return TRUE;
}

// Modify the current user's data in the DB
function user_modify($username, $password) {
    db_connect();

    // Prevent SQL injection attacks
    $username = db_escape_string($username);
    $password = db_escape_string($password);
    $hash = password_hash($password, PASSWORD_BCRYPT);

    $retval = db_query("INSERT into users (username, hash) VALUES " .
                       "('" . $username . "', '" . $hash . "')");
    if (!$retval) {
        return -1;
    }

    return TRUE;
}

// Get the array of the latest 10 params used by the user as stored in the DB
// Will always return an array
function user_get_params($userid) {
    db_connect();

    $userid = db_escape_string($userid);
    $retval = db_query("SELECT params FROM users WHERE userid=" . $userid);
    if (!$retval) {
        return -2;
    }

    $retval = db_fetch_assoc($retval);
    if (!$retval) {
        return -3;
    }

    $params = unserialize(base64_decode($retval['params']));
    if (!is_array($params)) {
        $params = array();
    }

    return $params;
}

// Add a param to the first place of the user's list
function user_add_param($userid, $param) {
    if (!$userid) {
        return -1;
    }

    db_connect();

    $userid = db_escape_string($userid);
    $params = user_get_params($userid);

    // Don't allow repeated items in the paarms array
    if (in_array($param, $params)) {
        return 0;
    }

    array_unshift($params, $param);
    while (count($params) > 10) {
        array_pop($params);
    }

    $params = base64_encode(serialize($params));
    $query = "UPDATE users SET params='" . $params . "' WHERE userid=$userid";
    $retval = db_exec($query);

    return $retval;
}

// Register the case for the specified user
// Case must have already been created in the DB and belong to the user
function user_register_case($caseid, $userid, $dirname, $fname, $solver, $params, $comments) {
    if (!$userid) {
        return -1;
    }

    db_connect();

    $caseid = db_escape_string($caseid);
    $userid = db_escape_string($userid);
    $dirname = db_escape_string($dirname);
    $fname = db_escape_string($fname);
    $solver = db_escape_string(basename($solver));
    $params = base64_encode($params);
    $comments = db_escape_string($comments);

    if (!case_belongs_to($caseid, $userid)) {
        return -1;
    }

    $query = "UPDATE cases SET creator_userid=$userid, creation_date='" . date('Y-m-d H:i:s') . "', " .
        "solver='$solver', dir='$dirname', file='$fname', params='$params', comments='$comments', status=1 WHERE id=$caseid";

    $retval = db_exec($query);

    if ($retval == FALSE) {
        die("ERROR with query: '$query': " + db_errorr());
    }

    return $retval;
}
