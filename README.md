Calculon
========

Calculon is a web interface to launch and monitor long-running tasks.
It was designed to launch
[MSC Nastran](http://www.mscsoftware.com/product/msc-nastran) jobs,
but it is pretty generic so it should work with pretty much anything
you can launch from a command line script.

Configuration
-------------
You can change configuration parameters in [inc/config.php](inc/config.php).
The code needs a database engine; both SQLite and MySQL/MariaDB are supported.
The code also needs a user authorization backend.
  
  * Local: Usernames and passwords are read from the local DB.
  * LDAP: Users must be authorized in the local DB, but credentials are
    checked against a LDAP server (your domain controller, normally).
    You must customize [inc/ldap_auth.php](inc/ldap_auth.php) for this
    to work.
    
The CSS code references a missing image file (`img/logo.png`). You are supposed
to place the logo of your company there and it will be shown as a kind of 
watermark in all the pages. 
    
The SQLite database in [dbs/calculon.sqlite](dbs/calculon.sqlite) includes a
test user named `test` with password `test`. This will work for `local`
authorization with a SQLite backend (which is the default).

System requirements
-------------------
The code has been tested in Linux x86_64 with an Apache server, but can probably
be easily adapted to other systems. The only platform-specific code lives
in [inc/system.php](inc/system.php).

The code was written for PHP 4, but should work fine in later versions of PHP
(including PHP 7).

Adding solvers
--------------
You can add support for solvers by adding executable scripts to the
[solvers](solvers) folder. There are several bash scripts in the folder
left for a reference.

Improvements
------------
Don't expect any. I haven't used it for a long time and am only uploading it
in case it is useful to anybody.

Points of improvement:

  * The LDAP vars should be moved to [inc/config.php](inc/config.php).

Drop me a line to joseba.gar@gmail.com if you find the code useful!