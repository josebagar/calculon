<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/inc/user.php');

// Check if the user session is active
// Comment for first-time user creation
if (user_logged_in() < 0) {
    header('Location: index.php');
    exit();
}

require_once(__DIR__.'/inc/config.php');
require_once(__DIR__.'/inc/html.php');

html_display_header('username');

db_connect();  // Connect to the SQL server, if not already done

// Fetch the full list of users
$retval = db_query('SELECT solver, dir, creator_userid, creation_date, params, comments, file FROM cases WHERE status!=0 ORDER BY id DESC');
if (!$retval) {
    return -2;
}

$n = 0;
while ($row = db_fetch_assoc($retval)) {
if (!file_exists($row['dir'])) {
    $status = 'Deleted';
} elseif (!file_exists($row['dir'] . '/.calculon_lock')) {
    $status = 'Done';
} else {
    $status = 'Analyzing';
}

if ($status != 'Deleted' || $_GET['showdel'] == 1) {
$n++;
if ($n == 1) {
?>
<table id="caselist">
    <tr class="grid table_header">
        <td style="width: 10em; text-overflow: ellipsis;">File name</td>
        <td style="width: 10em;">Solver</td>
        <td style="width: 10em;">Params</td>
        <td style="width: 10em;">Comments</td>
        <td style="width: 10em;">User</td>
        <td style="width: 10em;">Creation date</td>
        <td style="width: 10em;">Status</td>
    </tr>
    <?php
    }

    echo "\t\t<tr class=\"grid\">\n";
    if (file_exists($row['dir'])) {
        echo "\t\t\t<td><a href=\"analysis_status.php?id=" . substr($row['dir'],
                                                                    strlen($solver_job_dir . '/calculon_')) . "\">" . $row['file'] . "</a></td>\n";
    } else {
        echo "\t\t\t<td>" . $row['file'] . "</td>\n";
    }
    echo "\t\t\t<td>" . basename($row['solver'], '.solver') . "</td>\n";
    echo "\t\t\t<td>" . base64_decode($row['params']) . "</td>\n";
    echo "\t\t\t<td>" . $row['comments'] . "</td>\n";
    echo "\t\t\t<td>" . user_name($row['creator_userid']) . "</td>\n";
    echo "\t\t\t<td>" . $row['creation_date'] . "</td>\n";
    echo "\t\t\t<td>$status</td>\n";
    echo "\t\t</tr>\n";
    }
    }

    if ($n > 0) {
        echo "  </table>\n";
    } else {
        echo "No jobs to show...<br/>\n";
    }

    if ($_GET['showdel'] == 1) {
        echo "<a href=\"?showdel=0\">Hide deleted jobs</a>\n";
    } else {
        echo "<a href=\"?showdel=1\">Show deleted jobs</a>\n";
    }

    html_display_footer();
    exit();
