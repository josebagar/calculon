CREATE TABLE "cases" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
  `status`	int(11) DEFAULT '0',
  `solver`	text,
  `dir`	text,
  `creator_userid`	int(11) DEFAULT NULL,
  `creation_date`	text,
  `creation_date_unix`	text,
  `params`	text,
  `uploaded_files`	text,
  `file`	text,
  `comments`	TEXT
);

CREATE TABLE "users" (
  `userid`	INTEGER PRIMARY KEY AUTOINCREMENT,
  `username`	text,
  `hash`	text,
  `session_id`	text,
  `expiration_date`	bigint(20) DEFAULT NULL,
  `user_ip`	text,
  `user_permissions`	text,
  `params`	longtext
)

