#!/usr/bin/env python
# coding: utf-8

# Copyright 2011-2016 Joseba García Etxebarria
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Simple program to send an email through sendmail by JGE

Use as "%prog username jobid", where
    * username is the user who created the job (e.g. e8540)
    * joid is the id for the job
"""

import sys,socket
from subprocess import Popen, PIPE
from optparse import OptionParser
import sys,socket

# Support Python 2.4
if sys.version.split()[0] > '2.6':
        from email.mime.text import MIMEText
else:
        from email.MIMEText import MIMEText

parser = OptionParser(usage=__doc__)
(opts, args) = parser.parse_args()

if len(args) != 2:
    sys.stderr.write("Run with -h for help\n")
    sys.exit(-1)

# We don't usually need the FQDN, just the hostname
# Using the FQDN would lead to cookie issues and having to re-login
hostname = socket.gethostname().split('.')[0]

msg = MIMEText("Your job "+args[1]+"""has completed.
You can see how it went here:
http://"""+hostname+"/calculon/analysis_status.php?id="+args[1]+"""

--
The friendly calculus Slave
""")
msg["From"] = "calculon@"+hostname
msg["To"] = args[0]+"@mailserver.com"
msg["Subject"] = "Your job has completed."
p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
p.communicate(msg.as_string())
