/*
 Copyright 2011-2016 Joseba García Etxebarria

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

//Add input types to a form
function add_input(form_id, type, input_name) {
    var form = document.getElementById(form_id);
    if(form == null) {
        return null;
    }

    newinput=document.createElement('input');
    newinput.setAttribute('type', type);
    newinput.setAttribute('name', input_name);
    newinput.setAttribute('id', input_name);
    form.appendChild(newinput);
    form.appendChild(document.createElement('br'));

    return null;
}

// Increase the opacity value of an element by v until it reaches max_value
// with an interval of t
// If given a negative v, it will fade the element out, and if its opacity
// reaches 0.0, it will be hidden
function fade_element(id, v, max_value, t) {
    var element = document.getElementById(id);
    if(element == null || v == 0) {
        return null;
    }

    if(v > 0) {
        if(element.style.opacity < max_value) {
            element.style.opacity = parseFloat(element.style.opacity) + v;
            window.setTimeout(fade_element, t, id, v, max_value, t);
        }
    } else {
        if(element.style.opacity > max_value) {
            element.style.opacity = parseFloat(element.style.opacity) + v;
            window.setTimeout(fade_element, t, id, v, max_value, t);
        }
        if(parseFloat(element.style.opacity) <= 0.0) {
            element.style.visibility='hidden';
        }
    }
    return null;
}

// Hide an element by fading it out
function hide(id) {
    // Make sure the box ID exists
    var box = document.getElementById(id);
    if(box == null) {
        return null;
    }
    
    // Hide the element, it will be hidden automatically
    fade_element(id, -0.1, 0.0, 50);
    return null;
}
