/*
 Copyright 2011-2016 Joseba García Etxebarria

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

(function() {
    var dropTarget = document.getElementById("upload_table");
    var results = document.getElementById("upload_table_files");
    var caseid  = document.getElementById("caseid");
    var list = [];
    var totalSize = 0;
    var totalProgress = 0;
    
    //console.log(results);

    function init() {
        dropTarget.addEventListener("drop", handleDrop, false);
        dropTarget.addEventListener("dragover", handleDragOver, false);
        dropTarget.addEventListener("dragleave", handleDragLeave, false);
    }
    
    function handleDragOver(event) {
        event.stopPropagation();
        event.preventDefault();
    }

    function handleDragLeave(event) {
        event.stopPropagation();
        event.preventDefault();
    }
    
    function handleDrop(event) {
        event.stopPropagation();
        event.preventDefault();
        processFiles(event.dataTransfer.files);
    }
    
    function processFiles(filelist, event) {
        if(!filelist || !filelist.length || list.length) return;

        for(var i = 0; i < filelist.length && i < 150; i++) {
            list.push(filelist[i]);
            totalSize += filelist[i].size;
        }
        uploadNext();
    }
    
    function startUpload(file) {
        var result = document.createElement("tr");
        var name = document.createElement("td");
        var size = document.createElement("td");
        var status = document.createElement("td");
        var progress = document.createElement("progress");
        
        status.appendChild(progress);
        result.appendChild(name);
        result.appendChild(size);
        result.appendChild(status);
        results.appendChild(result);

        name.textContent = file.name;
        size.textContent = (file.size/1024).toFixed(1);
        
        if(file.size >= 500000000) {
            status.textContent = "MAX SIZE EXCEEDED";
            status.className = "fail";
            size.className = "fail";
            handleComplete(file.size);
        } else {
            uploadFile(file, status, progress);
        }
    }
    
    function uploadFile(file, status, progress) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", 'ajax_responder.php?id=1');
        xhr.setRequestHeader("Content-Type", file.type);
        xhr.setRequestHeader("X-File-Name", file.name);
        xhr.setRequestHeader("X-Case-ID", caseid.value);
        
        xhr.onload = function() {
            status.textContent = "Uploaded";
            status.className = "pass";
            handleComplete(file.size);
        };
        xhr.onerror = function() {
            status.textContent = "FAILED";
            status.className = "fail";
            handleComplete(file.size);
        };
        xhr.upload.onprogress = function(event) {
            progress.value = event.loaded / event.total;
        };
        xhr.upload.onloadstart = function(event) {
            progress.setAttribute("value", "0");
        };
        
        xhr.send(file);
    }
    
    function handleComplete(size) {
        totalProgress += size;
        uploadNext();
    }
    
    function uploadNext() {
        if(list.length) {
            startUpload(list.shift());
        }
    }
    
    init();
})();
