<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/inc/config.php');
require_once(__DIR__.'/inc/user.php');
require_once(__DIR__.'/inc/html.php');

// The user is not currently logged in
if (user_logged_in() < 0) {
    // If not, check if the user has entered its login data
    if (isset($_POST['username']) && (isset($_POST['password']))) {
        if (($retval = user_log_in($_POST['username'], $_POST['password'])) <= 0) {
            // Tried to login, but credentials are incorrect
            html_display_header('username');
            echo '<h2 class="error">Log-in error ('.$retval.').</h2>';
            html_display_login_form();
            html_display_footer();
            exit();
        } else {
            /* Login OK */
            /* We must refresh the page so that next time cookies are defined     */
            /* Otherwise, the page wouldn't know about the user data on the first */
            /* login. */
            header('Location: analyze.php');
            exit();
        }
    } else {
        // Not logged in nor tried to log in yet, display form
        html_display_header('username');
        html_display_login_form();
        html_display_footer();
        exit();
    }
} else {
    /* User is logged in */
    if (isset($_GET['logout'])) {
        user_log_out();
        header('Location: index.php');
        exit();
    }
}

// The user is logged in, display the reference list
header('Location: analyze.php');
exit();
