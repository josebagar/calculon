<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/inc/user.php');

// Check if the user session is active
// Comment for first-time user creation
if (user_logged_in() < 0) {
    header('Location: index.php');
    exit();
}

// Delete a user if asked to do so
if (isset($_GET['delid'])) {
    $username = user_name($_GET['delid']);
    // Will return a number if user doesn't exist
    if (is_string($username)) {
        user_del($username);
    }
}

require_once(__DIR__.'/inc/config.php');
require_once(__DIR__.'/inc/html.php');

html_display_header('username');

db_connect();  // Connect to the SQL server, if not already done

// Fetch the full list of users
$retval = db_query('SELECT userid, username, user_ip FROM users');
if (!$retval) {
    return -2;
}

?>
<table id="userlist">
    <tr class="grid table_header">
        <td>User Name</td>
        <td>Last IP</td>
        <td></td>
        <td></td>
    </tr>
    <?php
    while ($row = db_fetch_assoc($retval)) {
        echo "\t\t<tr class=\"grid\">\n";
        echo "\t\t\t<td>" . $row['username'] . "</td>\n";
        echo "\t\t\t<td>" . $row['user_ip'] . "</td>\n";
        echo "\t\t\t<td></td>\n";
        if ($row['userid'] != user_logged_in()) {
            echo "\t\t\t<td><a href=\"user_list.php?delid=" . $row['userid'] . "\">Remove user</a></td>\n";
        } else {
            echo "\t\t\t<td></td>\n";
        }
        echo "\t\t</tr>\n";
    }
    ?>
</table>
<?php

html_display_footer();
exit();
