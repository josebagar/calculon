<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/inc/user.php');
require_once(__DIR__.'/inc/html.php');
require_once(__DIR__.'/inc/misc.php');
require_once(__DIR__.'/inc/config.php');

/* This page is a bit special                                     */
/* You don't need to be logged in to be able to see what's inside */
/* but you have to be logged in to be able to delete a case.      */
/* This way, case results can be shared across the internet.      */

//If not given analysis ID, return to main page
if (!isset($_GET['id']) || empty($_GET['id'])) {
    header('Location: analyze.php');
    exit();
}

// Define the case vars
$analysis_dir = $solver_job_dir . '/calculon_' . $_GET['id'];
$lockfile = $analysis_dir . '/.calculon_lock';
$queuefile = $analysis_dir . '/.calculon_queued';
$pidfile = $analysis_dir . '/.calculon_pid';
$model_shot = $analysis_dir . '/model_shot.png';
$stdout = $analysis_dir . '/stdout';
$files = array();
foreach ($preview_extensions as $ext) {
    // List all the files of configured extensions
    $aux = glob($analysis_dir . '/*.' . $ext);
    $files = array_merge($files, $aux);
}

foreach ($_GET as $param => $value) {
    switch ($param) {
        case 'download';
            // Compress the TAR (if needed), then direct to FTP
            if (!file_exists($solver_job_dir . '/' . $tarname)) {
                exec('tar -cf ' . $solver_job_dir . '/' . $tarname . ' ' . $analysis_dir);
            }
            header('Location: ftp://' . $_SERVER['HTTP_HOST'] . '/pub/scratch/calculon_' . $_GET['id']);
            exit();
        case 'delete':
            if (user_logged_in() >= 0) {
                if (is_dir($analysis_dir)) {
                    unlink($stdout);
                    deltree($analysis_dir);
                }
                header('Location: analyze.php');
            } else {
                header('Location: login.php');
            }
            exit();
        case 'stop':
            if (user_logged_in() >= 0) {
                if (file_exists($pidfile)) {
                    $pid = intval(file_get_contents($pidfile));
                    if (!empty($pid)) {
                        exec('./' . $solver_script_dir . "/killtree.sh $pid");
                    }
                }
                header('Location: analysis_status.php?id=' . $_GET['id']);
            } else {
                header('Location: login.php');
            }
            exit();
    }
}

// Check the analysis dir exists
if (!file_exists($analysis_dir)) {
    html_display_header('', FALSE);
    echo "<h1>Analysis ID not valid</h1>";
} else {
    // Check the analysis file exists
    if (file_exists($queuefile)) {
        html_display_header('', TRUE);
        echo "<h1>Analysis queued</h1><br/>\n";
    } elseif (!file_exists($lockfile)) {
        html_display_header('', FALSE);
        echo "<h1>Analysis done</h1><br/>\n";
        echo "<div style=\"text-align: center;\">";
        // The user is logged in => show more options
        if (user_logged_in() >= 0) {
            echo "<a href=\"?delete&id=" . $_GET['id'] . "\">Delete case</a> - \n";
            echo "<a href=\"\">Sharing link</a> - \n";
        }
        if (file_exists($model_shot)) {
            echo "<a href=\"ftp://" . $_SERVER['HTTP_HOST'] . '/pub/scratch/calculon_' . $_GET['id'] . "/model_shot.png\">View model</a> - ";
        }
        echo "<a href=\"ftp://" . $_SERVER['HTTP_HOST'] . '/pub/scratch/calculon_' . $_GET['id'] . "\">Download case results</a>" .
            "<br /><br />\n";
        echo "</div>\n";
    } else {
        html_display_header('', TRUE);
        echo "<h1>Analyzing</h1><br/>\n";
        echo '<div style="text-align: center;">';
        if (user_logged_in() >= 0 && file_exists($pidfile)) {
            echo '<a href="?stop&id=' . $_GET["id"] . '">Stop analysis</a>' . "\n";
            $writeslash = TRUE;
        }
        if (file_exists($model_shot)) {
            if ($writeslash) {
                echo " - ";
            }
            echo "<a href=\"ftp://" . $_SERVER['HTTP_HOST'] . '/pub/scratch/calculon_' . $_GET['id'] . "/model_shot.png\">View model</a>\n";
        }
        echo "</div>\n";
    }
}

// Look for fatals in the f06 file
// FIXME: When f06file becomes too big this is VERY slow (remember, the CPU is running @ 100%)
#    $f06 = fopen($f06file, 'r');
$f06 = FALSE;
if ($f06 != FALSE) {
    while (!feof($f06)) {
        $f06line = fgets($f06);
        if (stristr($f06, 'fatal') != FALSE) {
            echo '<div style="text-align: center; font-weight: bold; color: rgb(255, 0, 0);">FATALs found!</div><br/>';
            break;
        }
    }
    fclose($f06);
}

// Display the file previews
?>
<div id="file_previews" dojoType="dijit.layout.AccordionContainer" style="height: 650px;">
    <?php
    // We'll always show stdout first, but it won't show by default
    html_display_file_contents($stdout, 'style="height: 650px; width: 90%; white-space: pre-wrap;"');
    foreach ($files as $file) {
        $ext = end(explode(".", $file));

        $extrahtml = '';
        if ($ext == $preview_default_ext) {
            $extrahtml = ' selected="true"';
        }

        html_peek_file_contents($file, 'style="white-space: pre-wrap;"' . $extrahtml,
                                $preview_length, $preview_length);
    }
    ?>
</div>
<?php
html_display_footer();
?>
