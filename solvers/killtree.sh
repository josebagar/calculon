#!/bin/bash

# Copyright 2011-2016 Joseba García Etxebarria
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Kill a process tree given its ancestor's PID
# Params:
# *     PID:    System PID for the process
# *     Signal: Signal to send with KILL
function killtree() {
    if [ $# -ne 2 ]; then
        echo "Error: must be given two params ($# given)"
        return 1
    fi

    local _pid=$1
    local _sig=${2-TERM}
    for _child in $(ps -o pid --no-headers --ppid ${_pid}); do
            killtree ${_child} ${_sig}
    done
    kill -${_sig} ${_pid}
}


killtree $1 "KILL"
