<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/inc/user.php');

// Check if the user session is active
// Comment for first-time user creation
if (user_logged_in() < 0) {
    header('Location: index.php');
    exit();
}

require_once(__DIR__.'/inc/config.php');
require_once(__DIR__.'/inc/html.php');

// If we've already entered data, process it
// Handle different $authtype values
if (isset($_GET['add'])) {
    $error = '<h2 class="error">Please fill in the form correctly</h2>';
    if (isset($_POST['username']) && !empty($_POST['username'])) {
        if ($authtype == 'ldap' || $authtype == 'cached') {
            // Generate some dummy password, it won't be used
            $username = $_POST['username'];
            $passwd = md5(rand());
            // Add the user and return to the login form
            if (user_add($username, $passwd) > 0) {
                header('Location: index.php');
                exit();
            } else {
                $error = '<h2 class="error">Error granting login rights to user</h2>';  // Unknown error
            }
        } else {
            if (isset($_POST['passwd']) && isset($_POST['passwd2'])) {
                if (!empty($_POST['passwd']) && !empty($_POST['passwd2'])) {
                    if ($_POST['passwd'] == $_POST['passwd2']) {
                        $username = $_POST['username'];
                        $passwd = $_POST['passwd'];
                        // Add the user and return to the login form
                        if (user_add($username, $passwd) > 0) {
                            header('Location: index.php');
                            exit();
                        } else {
                            $error = '<h2 class="error">Error adding user</h2>';  // Unknown error
                        }
                    }
                }
            }
        }
    }
    echo $error;
}

html_display_header('username');
if ($authtype == 'local') {
    html_display_user_add_form();
} else {
    html_display_user_grant_form();
}

html_display_footer();
exit();
