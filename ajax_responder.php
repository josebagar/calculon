<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/inc/user.php');
require_once(__DIR__.'/inc/analysis.php');
require_once(__DIR__.'/inc/html.php');

// Output upload debug info to a file
$debug = FALSE;

function debug($msg) {
    global $debug;

    if ($debug) {
        $file = fopen('/tmp/ajax_info.txt', 'w+');
        fputs($file, $msg . "\n");
        //fputs($file, "SERVER:\n");
        //fputs($file, print_r($_SERVER, True));
        //fputs($file, "GLOBALS:\n");
        //fputs($file, print_r($GLOBALS, True));
        fclose($file);
    }
}

// Check wether user is already logged in or refuse to handle request
$userid = user_logged_in();
if ($userid < 0) {
    // Refuse to upload
    debug("Refusing to handle AJAX request from non-logged in user");

    return -1;
}

// Implement the different behaviours
if (isset($_GET['id'])) {
    switch ($_GET['id']) {
        // HTML5 file upload
        case 1:
            // Read the case ID and check it belongs to the user
            // This part is common to all the AJAX events
            $caseid = intval($_SERVER['HTTP_X_CASE_ID']);
            if (case_belongs_to($caseid, $userid) == FALSE) {
                // Refuse to upload
                debug("Case $caseid doesn't belong to '$userid'\n");

                return -1;;
            }

            // File upload: add the uploaded file to the case
            $filename = $_SERVER['HTTP_X_FILE_NAME'];
            if (($retval = case_add_file($caseid, $filename)) !== TRUE) {
                // Refuse to upload
                debug("Error adding $filename to case $caseid ($retval)");

                return -1;
            }

            // Proceed with the upload
            $upload_dir = case_dir($caseid);
            if (empty($upload_dir)) {
                // Refuse to upload
                debug("Couldn't retrive case $caseid's directory");

                return -1;
            }

            // Remove file, if it already exists
            $path = $upload_dir . '/' . $filename;
            if (file_exists($path)) {
                debug("File $path already exists, unlinking");
                unlink($path);
            }

            file_put_contents($path, $HTTP_RAW_POST_DATA);

            break;
    }
}

return 0;
?>
