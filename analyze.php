<?php
// Copyright 2011-2016 Joseba García Etxebarria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

require_once(__DIR__.'/inc/config.php');
require_once(__DIR__.'/inc/user.php');
require_once(__DIR__.'/inc/html.php');
require_once(__DIR__.'/inc/analysis.php');

// The user is not currently logged in, redirect to the login page
$user_id = user_logged_in();
if ($user_id < 0) {
    header('Location: index.php');
    exit();
}

// The number of correctly uploaded files
$nfiles = 0;

if (!empty($_POST['caseid'])) {
    $caseid = $_POST['caseid'];
    $analysis_dir = case_dir($caseid);
    $case_files = case_get_uploaded_files($caseid);
    $nfiles = count($case_files);
}

if (!empty($_POST['solver'])) {
    $solver = $solver_script_dir . '/' . $_POST['solver'] . '.solver';
    if (!empty($_POST['caseid']) && !empty($_POST['main_file'])) {
        $analysis_file = $analysis_dir . '/' . $_POST['main_file'];

        // Check for nasty stuff
        if (dirname($solver) != $solver_script_dir) {
            echo "<h1>ERROR:</h1>";
            echo "Consistency check 2 failed";
            exit();
        }

        if (dirname($analysis_file) == $analysis_dir) {  // Prevent trickery
            if (file_exists($analysis_file) && is_executable($solver)) {
                user_add_param($user_id, base64_decode($_POST['params']));
                user_register_case($caseid, $user_id, dirname($analysis_file), basename($analysis_file), $solver,
                                   base64_decode($_POST['params']), $_POST['comments']);
                analyze(user_name($user_id),
                        substr($analysis_dir, strlen($solver_job_dir . '/calculon_')),
                        $analysis_file,
                        $solver,
                        base64_decode($_POST['params']));
                header('Location: analysis_status.php?id=' .
                       substr($analysis_dir, strlen($solver_job_dir . '/calculon_')));
                exit();
            }
        }
    }
}

// Analyze the uploaded file/s
// Handle the case where multiple files are uploaded
if ($nfiles > 0) {
    if ($nfiles == 1) {
        $analysis_file = $analysis_dir . '/' . $case_files[0];

        user_add_param($user_id, $_POST['params']);
        user_register_case($caseid, $user_id, dirname($analysis_file), basename($analysis_file), $solver,
                           $_POST['params'], $_POST['comments']);
        analyze(user_name($user_id),
                substr($analysis_dir, strlen($solver_job_dir . '/calculon_')),
                $analysis_file,
                $solver,
                $_POST['params']);
        header('Location: analysis_status.php?id=' .
               substr($analysis_dir, strlen($solver_job_dir . '/calculon_')));
        exit();
    } else {
        // Display the main analysis file selection dialog
        html_display_header();
        html_display_analysis_file_selector($_POST['caseid'],
                                            $_POST['solver'], $_POST['params'], $_POST['comments']);
        html_display_footer();
        exit();
    }
}

// Perform case cleanup
case_cleanup();

if (ini_get('file_uploads') == 0) {
    echo "<h1>Error: Your server is not configured to allow file uploads, quitting</h1>\n";
    die();
}

html_display_header();

html_display_systeminfo();

if (!isset($error) || empty($error)) {
    html_display_upload_form();
} else {
    echo "Error: " . $error;
}

html_display_footer();
